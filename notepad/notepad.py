from tkinter import *
from tkinter import filedialog, messagebox
from tkinter.ttk import Scrollbar, Checkbutton, Label, Button
import os
import sys


class NotePad(Tk):
    # 初始化
    def __init__(self):
        super().__init__()
        self.set_window()

    # 设置窗口界面
    def set_window(self):
        self.title('滇弦')
        max_width, max_height = self.maxsize()
        align_center = "800x600+%d+%d" % ((max_width-800)/2, (max_height-600)/2)
        self.geometry(align_center)
        self.iconbitmap('img/')


if __name__ == '__main__':
    app = NotePad()
    app.mainloop()
