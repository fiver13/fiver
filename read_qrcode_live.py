import cv2

# 摄像头实时识别二维码

# 初始化摄像头(捕获）
capture = cv2.VideoCapture(0)

# 初始化二维码
detector = cv2.QRCodeDetector()

while True:
    _, img = capture.read()
    # 检测，解码
    data, bbox, _ = detector.detectAndDecode(img)
    # 标识图片二维码的外框，识别数据
    # if bbox is not None:
    #     for i in range(len(bbox)):
    #         cv2.line(img, tuple(bbox[i][0]), tuple(bbox[(i + 1) % len(bbox)][0]), color=(255, 0, 0), thickness=2)
    if data:
        print("数据是：", data)

    cv2.imshow("qrcode reader", img)
    # title = ("摄像头拍摄".encode(encoding="GBK")).decode(encoding="GBK")
    # title = ("摄像头拍摄".encode(encoding="UTF-8")).decode(encoding="UTF-8")
    # print(title)    # 输出结果是”摄像头拍摄“，但是由于cv2的原因导致标题无法正确显示
    # cv2.im show(f"{title}", img)
    # cv2.im show(title, img)
    if cv2.waitKey(1) == ord("q"):
        break

# 释放资源
capture.release()
cv2.destroyAllWindows()
