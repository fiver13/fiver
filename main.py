from tkinter import Tk

'''
第一个tkinter测试练习
'''
win = Tk()
# 标题
win.title("tkinter")
# 获取整个屏幕大小
screen_width, screen_height = win.maxsize()
width = 300
height = 260
# 字符串设置窗口宽高和居中显示，x表示乘
align_str = '%dx%d+%d+%d' % (width, height, (screen_width-width)/2, (screen_height-height)/2)
# 设置窗口宽，高
win.geometry(align_str)
# 设置窗口是否可缩放
win.resizable(width=False, height=False)
win.mainloop()
