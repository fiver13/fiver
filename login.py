# coding=utf-8
import tkinter as tk
from tkinter import Tk, messagebox, Toplevel

'''
登录练习
'''
win = Tk()
win.title("tkinter")
screen_width, screen_height = win.maxsize()
width = 300
height = 260
align_str = '%dx%d+%d+%d' % (width, height, (screen_width - width) / 2, (screen_height - height) / 2)
win.geometry(align_str)
win.resizable(width=False, height=False)
# 添加标签
user_name_label = tk.Label(win, text='账号', font=('微软雅黑', 16))
# 设置位置
user_name_label.place(x=30, y=30)
user_pwd_label = tk.Label(win, text='密码', font=('微软雅黑', 16))
user_pwd_label.place(x=30, y=80)
# 输入框
input_user_name = tk.StringVar()
# input_user_name.set("输入用户名")
user_name_entry = tk.Entry(win, textvariable=input_user_name, font=('微软雅黑', 16), width=15)
user_name_entry.place(x=100, y=30)
input_user_pwd = tk.StringVar()
# input_user_pwd.set("输入密码")
user_pwd_entry = tk.Entry(win, textvariable=input_user_pwd, font=('微软雅黑', 16), width=15)
user_pwd_entry.place(x=100, y=80)


# 数据读取
def read_data():
    # 打开文件时需要设置编码方式，以防被打开的文件中有中文乱码
    with open('userInfo.txt', 'r', encoding='utf-8') as f:
        rows = f.readlines()
        user_info_dict = {}
        for row in rows:
            dict_list = row.strip().split(':')
            # print(dict_list)
            user_info_dict[dict_list[0]] = dict_list[1]
        f.close()
        return user_info_dict
        # for row in rows:
        #     print(row.strip().split(':'))
        # print(rows)   # 以列表的形式输出


def user_login():
    name = input_user_name.get()
    pwd = input_user_pwd.get()
    user_dict = read_data()
    if name != '' and pwd != '':
        if name in user_dict.keys():
            if pwd == user_dict[name]:
                # print('成功')
                messagebox.showinfo(title='成功', message='欢迎' + name + '到来')
            else:
                messagebox.showinfo(title='错误', message='密码错误')
                # print('密码错误')
        else:
            # print('用户名不正确')
            messagebox.showinfo(title='错误', message='用户名不存在')
    else:
        # print('账号，密码不能为空')
        # 可选showerror,showinfo,showwarning等
        messagebox.showinfo(title='错误', message='用户名，密码不能为空')
    # print(name, pwd)


# 弹出注册窗体
def pop_win():
    top = Toplevel()
    top.title('注册')
    top.geometry('300x260')
    '''
    账号，密码，确认密码，注册按钮
    布局方式---组件以何种方式组织 place 通过x,y坐标方式进行精确定位
    grid网格布局'''
    tk.Label(top, text='账号', width=10).grid(row=1, column=0, pady=10)
    user_name = tk.StringVar()
    tk.Entry(top, textvariable=user_name, width=15).grid(row=1, column=1, pady=10)
    tk.Label(top, text='密码', width=10).grid(row=2, column=0, pady=10)
    user_pwd = tk.StringVar()
    tk.Entry(top, textvariable=user_pwd, width=15).grid(row=2, column=1, pady=10)
    tk.Label(top, text='确认密码', width=10).grid(row=3, column=0, pady=10)
    user_confirm_pwd = tk.StringVar()
    tk.Entry(top, textvariable=user_confirm_pwd, width=15).grid(row=3, column=1, pady=10)

    # python支持在一个函数内部再定义一个函数（类似java内部类），其他语言C,Java...均不支持此功能
    def user_register():
        name = user_name.get()
        pwd = user_pwd.get()
        confirm_pwd = user_confirm_pwd.get()
        if pwd == confirm_pwd:
            # 设置写入时的字符编码，防止写入中文乱码
            with open('userInfo.txt', 'a', encoding='utf-8') as f:
                f.writelines("\n"+name + ":" + pwd)
                f.flush()
                f.close()
                messagebox.showinfo(title='成功', message='注册成功')
                # 销毁窗体
                top.destroy()
        else:
            messagebox.showinfo(title='失败', message='输入的密码不匹配')

    tk.Button(top, text='注册', width=10, command=user_register).grid(row=4, columnspan=2, pady=14)


# 按钮，处理事件
user_login_button = tk.Button(win, text='登录', font=('微软雅黑', 16), command=user_login)
user_login_button.place(x=30, y=140)
user_register_button = tk.Button(win, text='注册', font=('微软雅黑', 16), command=pop_win)
user_register_button.place(x=200, y=140)
win.mainloop()
# print(read_data())
