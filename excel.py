from openpyxl import *


def write_date():
    # 构建workbook
    work_book = Workbook()
    # 构建sheet
    sheet = work_book.active
    # 表格名字
    sheet.title = '学生信息表'
    # 向单元格写入数据
    sheet['A1'] = '姓名'
    sheet['B1'] = '电话'
    sheet['C1'] = '地址'
    sheet['D1'] = '性别'
    sheet['E1'] = '爱好1'
    sheet['F1'] = '爱好2'
    work_book.save(filename='stu.xlsx')


def write_data_content():
    # 加载表格
    work_book = load_workbook('stu.xlsx')
    sheet = work_book.active
    max_row = sheet.max_row
    # 写入数据的行数
    row = max_row+1
    # 构建写入数据
    data = ['张三', '1234567892', '湖北', '男', '文学', '美学']
    for data_col in range(1, 7):
        # cell方法(row=, column=, value=)
        sheet.cell(row=row, column=data_col, value=data[data_col-1])
    work_book.save(filename='stu.xlsx')
    # print(max_row)


# write_date()
write_data_content()
