from tkinter import Entry, Radiobutton, Checkbutton, IntVar, Listbox
from tkinter import Menu, Tk, Toplevel, Label, StringVar, END, Button
from tkinter.ttk import Combobox

from openpyxl import *

root = Tk()
root.title('菜单')
root.geometry('300x260+400+250')
# 自定义菜单
menu_bar = Menu(root)
# 创建菜单项
stu_bar = Menu(menu_bar, tearoff=0)  # 加上tear off=0是为了消除顶部的虚线，即菜单是否独立出来
list_value = StringVar()
list_box = Listbox(root, listvariable=list_value, width=40)


# 读取excel的数据 -> list返回类型
def read_excel() -> list:
    # 读取excel对象
    work_book = load_workbook('stu_info.xlsx')
    sheet = work_book.active
    stu_list = []
    # 读取excel中的数据
    for row in sheet.iter_rows(min_col=1, max_col=6,
                               min_row=2,
                               max_row=sheet.max_row):
        stu_temp = []
        for item in row:
            stu_temp.append(item.value)
        stu_list.append(stu_temp)
        # print(row)
    return stu_list


# 若仍用stu_list则会报Shadows name 'stu_list' from outer scope
stu_lists = read_excel()
for i in stu_lists:

    list_box.insert(END, i)
# tkinter三种布局，place(x=, y=);grid(row=, column=);pack(side=)
list_box.pack(side='top')


# 写入excel数据 stu: list参数类型
def write_excel(stu: list):
    # 重建excel文件，Workbook对象，需要把表头建好
    # work_book = Workbook()
    work_book = load_workbook('stu_info.xlsx')
    sheet = work_book.active
    max_row = sheet.max_row
    row = max_row + 1
    for data_cal in range(1, 7):
        sheet.cell(row=row, column=data_cal, value=str(stu[data_cal - 1]))
    work_book.save(filename='stu_info.xlsx')


# 添加学生信息
def pop_add_stu_win():
    stu_add_win = Toplevel()
    stu_add_win.title('添加学生')
    width = 300
    height = 300
    screen_width, screen_height = stu_add_win.maxsize()
    stu_add_win.geometry('%dx%d+%d+%d' % (width, height, (screen_width - width) / 2, (screen_height - height) / 2))
    # 添加组件
    Label(stu_add_win, text='姓名', width=10).grid(row=1, column=0, pady=10)
    user_name = StringVar()
    Entry(stu_add_win, textvariable=user_name, width=15).grid(row=1, column=1)
    Label(stu_add_win, text='电话', width=10).grid(row=2, column=0, pady=10)
    user_tel = StringVar()
    Entry(stu_add_win, textvariable=user_tel, width=15).grid(row=2, column=1)
    # 省份，下拉列表
    province_text = StringVar()
    Label(stu_add_win, text='省份', width=10).grid(row=3, column=0, pady=10)
    provinces = ['安徽', '江苏', '湖北', '湖南', '江西', '云南']
    province_comb = Combobox(stu_add_win,
                             textvariable=province_text, width=13, values=provinces, state='readonly')
    province_comb.current(3)
    province_comb.grid(row=3, column=1)
    # 性别，单选,默认不选中
    radio_btn_group = IntVar()
    # radio_btn_group = StringVar() # 则Radiobutton中value要为字符串，此时默认选中
    Label(stu_add_win, text='性别', width=10).grid(row=4, column=0, pady=10)
    # sticky属性表示对齐方式
    Radiobutton(stu_add_win, text='男', variable=radio_btn_group, value=1).grid(row=4, column=1, sticky='w')
    Radiobutton(stu_add_win, text='女', variable=radio_btn_group, value=2).grid(row=4, column=1, sticky='e')
    # 爱好,多选
    c_var1 = IntVar()
    c_var2 = IntVar()
    Label(stu_add_win, text='爱好', width=10).grid(row=5, column=0, pady=10)
    Checkbutton(stu_add_win, text='文学', variable=c_var1, onvalue=1, offvalue=0).grid(row=5, column=1, sticky='w')
    Checkbutton(stu_add_win, text='天文', variable=c_var2, onvalue=1, offvalue=0).grid(row=5, column=1, sticky='e')

    # 添加学生
    def add_stu():
        like_str2 = ''
        like_str1 = ''  # 解决变量在分配前引用的问题
        name = user_name.get()
        phone = user_tel.get()
        sex = radio_btn_group.get()
        address = province_text.get()
        like1 = c_var1.get()
        like2 = c_var2.get()
        if like1 == 1:
            like_str1 = '文学'
        if like2 == 1:
            like_str2 = '天文'
        stu = [name, phone, address, sex, like_str1, like_str2]
        write_excel(stu)

    Button(stu_add_win, text='添加学生', command=add_stu, width=10).grid(row=6, columnspan=2, pady=10)


# 创建菜单项
stu_bar.add_command(label='添加', command=pop_add_stu_win)
stu_bar.add_command(label='查询', command='')
menu_bar.add_cascade(label='学生', menu=stu_bar)
score_bar = Menu(menu_bar, tearoff=0)
score_bar.add_command(label='查询', command='')
score_bar.add_command(label='添加', command='')
menu_bar.add_cascade(label='成绩', menu=score_bar)
# 配置项设置
root.config(menu=menu_bar)
root.mainloop()
# print(read_excel())
