import qrcode

# 数据
data = "https://space.bilibili.com/418246941"
# 文件
filename = 'qrcode.png'
# 生成二维码
img = qrcode.make(data)
# 保存文件
img.save(filename)
