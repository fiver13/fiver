#!/usr/bin/python

import sqlite3

conn = sqlite3.connect('Account.db')
print("Opened database successfully")
c = conn.cursor()
# 创建表
c.execute('''CREATE TABLE Account
       (id INTEGER PRIMARY KEY     NOT NULL,
       name          TEXT    NOT NULL,
       price            REAL     NOT NULL,
       type        TEXT);''')
# 插入数据
# c.execute("INSERT INTO COMPANY (ID,NAME,AGE,ADDRESS,SALARY) \
#       VALUES (1, 'Paul', 32, 'California', 20000.00 )")
#
# c.execute("INSERT INTO COMPANY (ID,NAME,AGE,ADDRESS,SALARY) \
#       VALUES (2, 'Allen', 25, 'Texas', 15000.00 )")
#
# c.execute("INSERT INTO COMPANY (ID,NAME,AGE,ADDRESS,SALARY) \
#       VALUES (3, 'Teddy', 23, 'Norway', 20000.00 )")
#
# c.execute("INSERT INTO COMPANY (ID,NAME,AGE,ADDRESS,SALARY) \
#       VALUES (4, 'Mark', 25, 'Rich-M ', 65000.00 )")
# 更新数据
# c.execute("UPDATE COMPANY set ADDRESS = 'Rich-M' where ID=4")
# conn.commit()
# print("Total number of rows updated :", conn.total_changes)
# 查询数据
# cursor = c.execute("SELECT id, name, address, salary  from COMPANY")
# for row in cursor:
#     print("ID = ", row[0])
#     print("NAME = ", row[1])
#     print("ADDRESS = ", row[2])
#     print("SALARY = ", row[3], "\n")
print("Table created successfully")
conn.commit()
conn.close()
